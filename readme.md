```
USAGE
       gitu [option]

OPTIONS
          get [spaceName] [repo]
              Gets the last commit from a repo.

          set [message]
              Sends local changes to the remote repo.

          addSpace [spaceName] [spaceAddress]
              Adds  a  space  where  to look for repos when referenced by their
              name.

          removeSpaces [spaceNames]
              Removes spaces.

          listSpaces
              Lists existing spaces.

FORMATS
          repo

              • repo

              • git@site.com:space/repo.git

              • https://site.com/space/repo.git

              • empty = current dir

          spaceName

              • spaceName

              • empty = 0-default

          spaceAddress

              • git@site.com:user

              • https://site.com/user

          message

              • message

              • empty = current UTC time (YYYY-MM-DD.S)
