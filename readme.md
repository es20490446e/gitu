```
USAGE
       gitu [option]

OPTIONS
          get [name/address]
              Gets  the  last commit from the remote repository. If no name or
              address are provided, the current dir is assumed.

          set [message]
              Sends local updates to the remote repository. If no  message  is
              provided, a timestamp is used instead.

          repo [address]
              Sets  the  place  where  to look for projects when referenced by
              their name. For example:

                    • git@website.com:user

                    • https://website.com/user
